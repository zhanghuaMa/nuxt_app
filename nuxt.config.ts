// @ts-ignore
// @ts-ignore
export default defineNuxtConfig({
    target: "static",
    modules: ['@element-plus/nuxt', '@nuxt/content'], // 飘红，暂未解决
    elementPlus: { /** Options */ },
    content: {
        // https://content.nuxtjs.org/api/configuration
    },
    typescript: {
        strict: true
    },
    app: {
        pageTransition: { name: 'page', mode: 'out-in' },
        layoutTransition: { name: 'layout', mode: 'out-in' },
        head: {
            charset: 'utf-8',
            viewport: 'width=device-width, initial-scale=1, maximum-scale=1',
            title: '随缘日记',
            meta: [
                // <meta name="description" content="My amazing site">
                { name: 'description', content: '随缘日记的前端博客' },
                { name: 'keywords', content: 'nuxt3 vue3 element plus' }
            ]
        }
    },
    css: ['element-plus/dist/index.css', 'assets/css/app.less'],
})
